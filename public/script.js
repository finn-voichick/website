const tooltipTriggerList =
  [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
const tooltipList =
  tooltipTriggerList.map(
    tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl)
  );

function onClick(event) {
  event.preventDefault();
  const a = event.target;
  a.href = "mailto:finn@umd.edu";
  a.textContent = "finn@umd.edu";
  a.removeEventListener("click", onClick);
}
document.getElementById("email").addEventListener("click", onClick);
