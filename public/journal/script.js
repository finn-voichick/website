let lastEdit = null;
let writingTime = 0;
let animate = null;
let saved = false;

const targetWords = 750;
const targetTime = 20 * 60;

const count = document.getElementById("count");
const plural = document.getElementById("plural");
const edit = document.getElementById("edit");
const progress = document.getElementById("progress");
const bar = document.getElementById("bar");
const minutes = document.getElementById("minutes");
const saveButton = document.getElementById("save");
edit.addEventListener("beforeinput", beforeInput);
edit.addEventListener("input", input);

window.getSelection().selectAllChildren(document.getElementById("start"));


/**
 * @param {string} w
 */
function isWord(w) {
	return /\w/.test(w);
}

function wordCount() {
	return edit.innerText.split(/\s+/).filter(isWord).length
}

/**
 * @param {InputEvent} event
 */
function input(event) {
	const timestamp = event.timeStamp
	if (lastEdit === null) {
		lastEdit = timestamp;
	} else {
		const interval = timestamp - lastEdit;
		if (interval > 0) {
			lastEdit = timestamp;
			if (interval <= 3000) {
				writingTime += interval
			}
		}
	}
	saved = false;
	minutes.innerText = Math.floor(writingTime / 1000 / 60).toString()
	const n = wordCount();
	count.innerText = n.toString();
	plural.hidden = n === 1;

	const totalProgress = currentProgress() * 100.0;
	progress.ariaValueNow = totalProgress.toString();
	bar.style.width = `${totalProgress}%`;
	clearTimeout(animate)
	bar.classList.remove("progress-bar-striped");
	animate = setTimeout(timeout, 3000)
}

function currentProgress() {
	const timeProgress = (writingTime / 1000) / targetTime;
	const wordProgress = wordCount() / targetWords;
	return Math.min(Math.max(timeProgress, wordProgress, 0.0), 1.0);
}

/**
 * @param {Element} element
 * @param {string} from
 * @param {string} to
 */
function replaceClass(element, from, to) {
	const classes = element.classList;
	classes.remove(from);
	classes.add(to);
}

function timeout() {
	if (currentProgress() < 1.0)
		bar.classList.add("progress-bar-striped");
	else {
		replaceClass(saveButton, "btn-secondary", "btn-primary")
		replaceClass(bar, "bg-secondary", "bg-success")
	}
}

/** @param {InputEvent} event */
function beforeInput(event) {
	for (const range of event.getTargetRanges()) {
		const start = range.startContainer;
		if (start.parentNode === edit && !start.previousSibling) {
			const end = range.endContainer;
			if (end.parentNode === edit && !end.nextSibling) {
				event.preventDefault();
			}
		}
	}
}

const currentDate = new Date();

// Function to get the ISO week number
const tempDate = new Date(currentDate.getTime());
tempDate.setDate(tempDate.getDate() + 4 - (tempDate.getDay() || 7));
const yearStart = new Date(tempDate.getFullYear(), 0, 1);
const weekNumber = Math.ceil((((tempDate - yearStart) / 86400000) + 1) / 7);

// Calculate the ISO year
const isoYear = (currentDate.getMonth() === 0 && weekNumber > 50) ? currentDate.getFullYear() - 1 : (currentDate.getMonth() === 11 && weekNumber === 1) ? currentDate.getFullYear() + 1 : currentDate.getFullYear();

// Get the day of the week (1 = Monday through 7 = Sunday)
const isoDay = (currentDate.getDay() + 6) % 7 + 1;

// Format the output
const isoDate = `${isoYear}-W${String(weekNumber).padStart(2, '0')}-${isoDay}`;

document.getElementById("date").innerText = isoDate;

function save() {
	const blob = new Blob([edit.innerHTML]);
	const url = URL.createObjectURL(blob);
	const link = document.createElement("a");
	link.href = url;
	link.download = `journal-${isoDate}.html`
	link.click();
	URL.revokeObjectURL(url);
	saved = true;
}

saveButton.addEventListener("click", save);

/**
 * @param {BeforeUnloadEvent} event
 */
function confirm(event) {
	if (!saved && wordCount() > 0) {
		event.preventDefault();
	}
}

window.addEventListener("beforeunload", confirm);