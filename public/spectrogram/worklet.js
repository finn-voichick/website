class ForwardWorklet extends AudioWorkletProcessor {
	/**
	 * @param {Float32Array[][]} inputs
	 */
	process(inputs) {
		const input = inputs[0][0].buffer;
		this.port.postMessage(input, [input]);
	}
}

console.log("Sample rate", sampleRate);

registerProcessor("forward-processor", ForwardWorklet);